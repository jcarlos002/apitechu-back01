package com.techu.backend.controller;

import com.techu.backend.model.ProductoModel;
import com.techu.backend.model.ProductoPrecioModel;
import com.techu.backend.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${url.base}")
public class ProductoController {

    @Autowired
    private ProductoService productoService;

    @GetMapping("")
    public String home()  {
        return "API REST Tech U! v1.0.0";
    }

    // GET todos los productos (collection)
    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productoService.getProductos();
    }

    // GET a un producto por Id (instancia)
    @GetMapping("/productos/{id}")
    // Si en responseEntity se informa ResponseEntity<ProductoModel> solo se podrian devolver variables de ese tipo
    public ResponseEntity getProductoById(@PathVariable String id){
        ProductoModel pr = productoService.getProductoById(id);
        if (pr == null){
            // no existe el producto
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
    //    return ResponseEntity.ok(pr);
        return new ResponseEntity(pr,HttpStatus.OK);
    }

    // POST para crear productos
    @PostMapping("/productos")
    public ResponseEntity<String> postProducto(@RequestBody ProductoModel newProduct) {

        productoService.addProducto(newProduct);
        return new ResponseEntity<>("Producto creado correctamente",HttpStatus.CREATED);
    }

    // PUT para actualizar un producto
    @PutMapping("/productos/{id}")
    public  ResponseEntity putProductoById(@PathVariable String id, @RequestBody ProductoModel productoModel) {
        ProductoModel pr = productoService.getProductoById(id);
        if (pr == null){
            // no existe el producto
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productoService.updateProductoById(Integer.parseInt(id),productoModel);
        return new ResponseEntity<>("Producto actualizado correctamente",HttpStatus.OK);
    }

    // GET subrecurso
    @GetMapping("/productos/{id}/users")
    public ResponseEntity getUsers(@PathVariable String id) {
        ProductoModel pr = productoService.getProductoById(id);
        if (pr == null){
            // no existe el producto
            return new ResponseEntity<>("Producto no encontrado.",HttpStatus.NOT_FOUND);
        }
        if (pr.getUsers() !=null) {
            return ResponseEntity.ok(pr.getUsers());
        }
        else{
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    // DELETE para borrar un producto
    @DeleteMapping("/productos/{id}")
    public ResponseEntity deleteProductoById(@PathVariable String id) {
        ProductoModel pr = productoService.getProductoById(id);
        if (pr == null){
            // no existe el producto
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productoService.removeProductoById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT); // 204
    }

    // PATCH para actualizar parcialmente un producto
    @PatchMapping("/productos/{id}")
    public ResponseEntity patchProductoById(@PathVariable String id,
                                    @RequestBody ProductoPrecioModel productoPrecioModel) {
        ProductoModel pr = productoService.getProductoById(id);
        if (pr == null){
            // no existe el producto
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }

        productoService.updateProductoPrecioById(id,productoPrecioModel.getPrecio());
        return new ResponseEntity<>("Producto actualizado correctamente",HttpStatus.OK);
    }


}
